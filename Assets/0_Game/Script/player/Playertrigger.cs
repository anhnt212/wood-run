﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Playertrigger : MonoBehaviour
{
    [SerializeField] private Transform Spawnbrickpoint;
    [SerializeField] private Transform Spawnbrickpointfoot;
    [SerializeField] private GameObject brickfrefabs;
    [SerializeField] private Transform Playerlocation;
    [SerializeField] private Rigidbody Rb_player;
    
    private List<GameObject> listbrick = new List<GameObject>();
    private List<GameObject> ListBirckwin = new List<GameObject>();
    private Vector3 brickaddposition;
    private int listcount=0 ;
    private int listcountwin = 0;
 
    private void Start()
    {
        

    }
    private void OnTriggerEnter(Collider other)
    {

        if (other.CompareTag("Brick"))
        {
            
            Destroy(other.gameObject);

            brickAdd();
        }
        //------------------------------------
        if (other.CompareTag("Boxdie"))
        {
            int i;
            listcount = listbrick.Count;
            for (i=0;i<listcount;i++)
            {

                BrickRemove();
            }
            GameManager.ins.StopPlayer();
            GameManager.ins.Stopgame();
            GameManager.ins.player_Foward.animator.SetBool("Die", true);
            StartCoroutine(gamelose());


        }
        //-------------------------------------
        if (other.CompareTag("Boxcheck"))
        {
           if(listbrick.Count > 0)
            {
                buildbringe();

            }
            else
            {
                GameManager.ins.StopPlayer() ;
                Rb_player.useGravity = true;
                StartCoroutine(gamelose());
            }

        }
        //-------------------------------------
        if (other.CompareTag("Checkwin"))
        {
            GameManager.ins.checkwin = true;
        }
        //--------------------------------------
        if (other.CompareTag("Boxcheckend"))
        {
            if (listbrick.Count > 0)
            {
                buildbringe();
            }
            else
            {

                GameManager.ins.StopPlayer();
                int i;
                listcountwin = ListBirckwin.Count;
                for (i = 0; i < listcount; i++)
                {

                    Destroy(ListBirckwin[ListBirckwin.Count - 1]);
                    listbrick.RemoveAt(ListBirckwin.Count - 1);

                }


                GameManager.ins.Win();
               
                

            }           
        }
        //------------------------------------
        if (other.CompareTag("Gate"))
        {
            TypeGateNumber typegate = other.gameObject.GetComponent<Gate_info>().typeGatenumber ;
            int intnumbergate = other.gameObject.GetComponent<Gate_info>().number;

            switch (typegate)
            {
                case TypeGateNumber.cong:
                    cong(intnumbergate);
                    break;

                case TypeGateNumber.tru:
                    tru(intnumbergate);
                    break;
                case TypeGateNumber.nhan:
                    nhan(intnumbergate);
                    break;
                case TypeGateNumber.chia:
                    chia(intnumbergate);
                    break;

            }
            
        }


    }
    


    void brickAdd()
    {
        listcount = listbrick.Count;
        brickaddposition = Spawnbrickpoint.position + new Vector3(0,0.1f * listcount,0)  ;

        GameObject Brick    = Instantiate(brickfrefabs, brickaddposition, Quaternion.identity);

        Brick.transform.SetParent(Spawnbrickpoint);
        listbrick.Add(Brick);

    }
    void BrickRemove()
    {
        
        Destroy(listbrick[listbrick.Count - 1]);
        listbrick.RemoveAt(listbrick.Count - 1);

        // listcount = listbrick.Count;
        //0 1 2 3 4 
        // 1 2 3 4 5


    }
    void buildbringe()
    {
        BrickRemove();

        GameObject Brick = Instantiate(brickfrefabs, Spawnbrickpointfoot.position, Quaternion.identity);
        ListBirckwin.Add(Brick);

    }
    IEnumerator gamelose()
    {
        yield return new WaitForSeconds(2f);
        GameManager.ins.lose();
    }
    
    void cong(int numbergate)
    {
        int i;
        for (i=0; i<numbergate;i++)
        {
            brickAdd();
        }

    }
    void tru(int numbergate)
    {

        int i;
        int a = numbergate;
        if(listbrick.Count <numbergate)
        {
            a = listbrick.Count;
        }

        for (i = 0; i < a; i++)
        {
           BrickRemove();
        }

    }
    void nhan(int numbergate)
    {
        int i;
        int a = listbrick.Count * numbergate -listbrick.Count;
        for (i = 0; i < a; i++)
        {
            brickAdd();
        }



    }
    void chia(int numbergate)
    {
        int i;
        int b = listbrick.Count - (listbrick.Count - listbrick.Count % numbergate) / numbergate;
        


        for (i = 0; i < b; i++)
        {
            BrickRemove();
        }

    }






}
