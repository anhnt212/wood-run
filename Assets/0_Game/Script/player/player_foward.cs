using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class player_foward : MonoBehaviour
{
    public float speed;
    public Animator animator;

    void FixedUpdate()
    {
        if (GameManager.ins.checkwin == false)
        {
            if (speed == 0)
            {
                
                animator.SetBool("Running", false);
            }
            else
            {
                animator.SetBool("Running", true);
                animator.SetBool("Idle", false);
            }
          
            transform.position += transform.forward * speed * Time.fixedDeltaTime;


        }
        else
        {
            transform.position += ( transform.forward + new Vector3(0,1f,0) )* speed * Time.fixedDeltaTime;

            if (speed == 0)
            {
                animator.SetBool("Victory", true);
                animator.SetBool("Running", false);
                animator.SetBool("Idle", false);
            }
            else
            {
                animator.SetBool("Running", true);
                animator.SetBool("Idle", false);
            }
           

        }
        

    }
}
