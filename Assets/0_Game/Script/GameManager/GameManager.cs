using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class GameManager : SingletonMonoBehaviour<GameManager>
{
    public player_foward player_Foward;
    public GameObject CanvasLose;
    public GameObject Canvaswin;
    public float speedcurrent ;
    public bool checkwin = false;
    public bool stopgame =false ;
    public bool startgame = false;

    private void Start()
    {
        CanvasLose.SetActive(false);
        Stopgame();
        StopPlayer();
        startgame = false;
    }

    public void StopPlayer()
    {   
        speedcurrent = player_Foward.speed;
        player_Foward.speed = 0;


    }

  public  void ResumePlayer()
    {
        stopgame = false;
        player_Foward.speed = speedcurrent;

    }

   public void Win()
    {
        Canvaswin.SetActive(true);

    }

   public void lose()
    {
        CanvasLose.SetActive( true);
        

    }
    

    public void Stopgame()
    {
        stopgame = true;
    }



}
