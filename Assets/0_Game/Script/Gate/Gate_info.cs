using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;



public class Gate_info : MonoBehaviour
{
    public TextMeshPro numbertext;

    public TypeGateNumber typeGatenumber;

    public int number;
    private string stringtype;

    private void Start()
    {

        switch (typeGatenumber)
        {
            case TypeGateNumber.cong:
                stringtype = "+";
                break;
            
            case TypeGateNumber.tru:
                stringtype = "-";
                break;

            case TypeGateNumber.nhan:
                stringtype = "x";
                break;

            case TypeGateNumber.chia:
                stringtype = ":";
                break;


        }




        numbertext.text = stringtype+ " " +  number.ToString();

    }



}
